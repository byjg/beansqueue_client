# Beansqueue Client

Enfileirar um job em uma fila Beanstalk.

```php
<?php

$queue = \BeansqueueClient\PheanstalkEnqueuer::factory();

$queueId = $queue->put(
    'queuename',               // The queuename 
    [
        "data"=>"value",       // The payload as array
        "data2"=>"value2"
    ],
    60,                        // Timeout (opcional)
    \Pheanstalk\PheanstalkInterface::DEFAULT_PRIORITY,  // Opcional
    0                          // Delay (Opcional)
);
```

