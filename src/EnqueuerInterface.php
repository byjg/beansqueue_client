<?php

namespace BeansqueueClient;

interface EnqueuerInterface
{
    /**
     * Insere uma mensagem em uma determinada fila
     *
     * @param string $queue
     * @param array $payload
     * @param integer $timeout
     * @return int
     */
    public function put($queue, $payload, $timeout = 60);
}
