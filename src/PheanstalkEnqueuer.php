<?php

namespace BeansqueueClient;

use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;

/**
 * Class PheanstalkEnqueuer
 * @package BeansqueueClient
 */
class PheanstalkEnqueuer implements EnqueuerInterface
{
    const GLOBAL_TIMEOUT = 600;

    /**
     * @var PheanstalkInterface
     */
    private $pheanstalk;

    /**
     * PheanstalkEnqueuer constructor.
     *
     * @param \Pheanstalk\PheanstalkInterface|null $pheanstalk
     */
    public function __construct(PheanstalkInterface $pheanstalk)
    {
        $this->pheanstalk = $pheanstalk;
    }

    public static function factory($server)
    {
        $pheanstalk = new Pheanstalk($server);
        return new PheanstalkEnqueuer($pheanstalk);
    }

    /**
     * @inheritDoc
     */
    public function put(
        $queue,
        $payload,
        $timeout = 60,
        $priority = PheanstalkInterface::DEFAULT_PRIORITY,
        $delay = PheanstalkInterface::DEFAULT_DELAY
    ) {

        $payload = [
            'data' => $payload,
            'timeout' => $timeout
        ];

        $enqueueId = $this->pheanstalk
            ->useTube(ltrim(str_replace('\\', '.', $queue), '\\'))
            ->put(json_encode($payload), $priority, $delay, self::GLOBAL_TIMEOUT);

        return $enqueueId;
    }
}
